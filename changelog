guncat (2.02.00)

  * Requires bobcat >= 6.07.00 and icmake >= 13.00,03. 

  * Changed ArgConfig::None into ArgConfig::NoArg.

  * Building uses a SPCH and multi-compilation.

  * The compiler is called using the value of the ${ICMAKE_CPPSTD} environment
    variable specifying the C++ standard to use. 

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sun, 12 Jan 2025 20:50:02 +0100

guncat (2.01.00)

  * GPG sends its output to cout, instead of to a pipe

  * Added option --gpg-messages to specify where GPG should send its messages
    to (instead of to the std. error stream)

  * Added option --dots writes a dot (.) to the standard error stream after
    processing each block of 8192 input lines

  * Modified option --passphrase: discontinued the short option (-p), and
    requires a separate file containing the passphrase to use.

  * Removed the options --less, --pipe and --write, as all output is sent
    the the standard output stream

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Fri, 14 Oct 2022 14:16:22 +0200

guncat (2.00.03)

  * Ready for libbobcat6

  * Added 'c++std' defining the c++ standard to use for
    compilation. Compilation commands also use -Werror

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sun, 11 Sep 2022 11:18:05 +0200

guncat (2.00.02)

  * Removed -q from guncat's build script

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sat, 26 Jun 2021 15:03:45 +0200

guncat (2.00.01)

  * Man-page cosmetics

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Fri, 25 Dec 2020 10:24:46 +0100

guncat (2.00.00)

  * Removed options --errors-OK, --locate-keys and --gpg-no-batch

  * Before calling gpg the encountered PGP MESSAGE section is inspected for
    correctness while it's written to a temporary file. If the PGP MESSAGE
    section contains errors gpg is not called. 

  * Added option --no-errors terminating guncat if a PGP MESSAGE section
    contains errors or if gpg returns a non-zero exit value.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Wed, 23 Dec 2020 21:41:17 +0100

guncat (1.02.01)

  * Bobcat 5.04.00's Pipe class defines a close member closing both ends of
    the pipe. This member is now called from GPipe's destructor.

  * Fixed some typos in the man-page
 
 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sat, 07 Mar 2020 14:45:58 +0100

guncat (1.02.00)

  * New options: errors-OK, time-limit; disused options: gpg-no-batch,
    locate-keys 

  * Refined the Decryptor's handlePGP / insertPGPsection implementations

  * The passphrase is requested at the start of the program to avoid
    interference with output to the std. output stream
    
  * Updated options passed to gpg to gpg version 2.1, see the description of
    the --passphrase-fd option in the gpg(1) manual page.

  * Using compilation option --std=c++2a

  * Updated usage and man-page

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sun, 11 Nov 2018 16:03:27 +0100

guncat (1.01.03)

  * Migrated from Github to Gitlab.
  * Updated the C++ standard to use to c++17.
  * Added header-precompilation to icmconf.
  * Removed comment from CLASSES

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Thu, 21 Jun 2018 19:18:26 +0530

guncat (1.01.02)

  * Updated the build script to icmake 8.00.04.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sat, 12 Dec 2015 15:12:24 +0100

guncat (1.01.01)

  * Kevin Brodsky observed that the installation scripts used 'chdir' rather
    than 'cd'. Fixed in this release.

  * Kevin Brodsky also observed that the combined size of all precompiled 
    headers might exceed some disks capacities. The option -P was added to the
    ./build script to prevent the use of precompiled headers.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Mon, 05 Oct 2015 21:25:25 +0200

guncat (1.01.00)

 * Update to version 1.00.00 after being operational for over one year without
   issues. 

 * Added 'build uninstall'. 

 * Updated  'INSTALL'

 * Guncat depends on libbobcat >= 4.00.00 (see 'required')

 * Standardized the (de)installation procedures

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sun, 04 Oct 2015 10:32:32 +0200

guncat (0.92.00)

  * The GPG passphrase is now requested only once. It is only requested again
    if a provided passphrase was incorrect.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Tue, 27 May 2014 15:57:34 +0200

guncat (0.91.00)

  * The GPG passphrase is read from /dev/tty, and not from the std. input
    stream (cin) anymore

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sun, 04 May 2014 10:55:53 +0200

guncat (0.90.00)

  * Initial completion of the program and man-page.

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Sat, 03 May 2014 11:48:43 +0200

guncat (0.00.00)

  * Preparing the sourceforge archive, and finalizing release 0.00.00

 -- Frank B. Brokken <f.b.brokken@rug.nl>  Wed, 30 Apr 2014 15:10:18 +0200
