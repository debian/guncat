#include "decryptor.ih"

void Decryptor::clearFiles() const
{
    error_code ec;
    resize_file(d_pgpMessage->fileName(), 0, ec);
    d_pgpMessage->clear();
    d_pgpMessage->seekp(0);
}
