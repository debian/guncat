#include "decryptor.ih"

void Decryptor::decrypt(PGPSection const &pgpSection) const
{
    size_t attempt = 0;

    while (true)
    {
        d_pgpMessage->seekg(0);
            
        int ret;
        if 
        (
            ret = d_gpgHandler.process(d_pgpMessage->fileName());
            ret == 0
        )
            break;                    

        if (attempt++ == 3)
        {
            auto [begin, end] = pgpSection.range();

            string msg{ "In " + g_filename + " lines " + 
                        to_string(begin) + " to " + to_string(end) +
                        ": cannot decrypt PGP section" };

            if (not d_options.noErrors())   // allow unkown passphr.
            {
                *g_verbose << msg << endl;
                cout << msg << '\n';
                break;
            }
            error(ret, msg);
        }
        d_gpgHandler.getPassphrase(attempt);
    }
}


