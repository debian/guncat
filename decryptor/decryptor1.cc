#define XERR
#include "decryptor.ih"

Decryptor::Decryptor(GPGHandler &gpgHandler)
:
    d_options(Options::instance()),
    d_gpgHandler(gpgHandler),
    d_pgpMessage(new TempStream)
{
    setVerbose();

    if (d_options.quotedPrintable())        // file to receive the decrypted
        d_gpgHandler.setDecryptedName("-"); // section ...

    else                                    // ... if --quoted-printable is
    {                                       // NOT requested  
        d_decrypted.reset(new TempStream);
        d_gpgHandler.setDecryptedName(d_decrypted->fileName());  
    }
}




