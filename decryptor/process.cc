#define XERR
#include "decryptor.ih"

    // in: current input file, at 'PGP MESSAGE'
void Decryptor::process(istream &in)    // , ostream &out)
{
xerr("");
                                                    // obtain the PGP MESSAGE
    PGPSection pgpSection{ in, *d_pgpMessage };

    if (g_verbose)
         *g_verbose << fileInfo() << ": PGP HEADER" << endl;

    clearFiles();                                   // resize d_pgpMessage and
                                                    // d_decrypted to 0-size

                                                    // get the (complete)
    if (pgpSection.complete())                      // PGP section
    {
        if (not d_options.pgpRanges())              // decrypt it unless only
            decrypt(pgpSection);                    // ranges are requested
        return;
    }
                                                    // unless suppressed:
    if (not d_options.skipIncomplete())             // show the INcomplete
        cout << d_pgpMessage->rdbuf();              // PGP MESSAGE
}







