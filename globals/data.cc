#include "globals.ih"

size_t g_lineNr;

string g_filename;
string g_beginPGP{ "-----BEGIN PGP MESSAGE-----" };
string g_endPGP{ "-----END PGP MESSAGE-----" };

ostream *g_verbose = 0;        // no messages if 0
