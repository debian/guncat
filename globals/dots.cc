#include "globals.ih"

namespace
{
    void noDots()
    {}

    void doDots()
    {
        if ((g_lineNr & ((1 << 13) - 1)) == 0)
            cerr.put('.');
    }
}

void (*g_dots)() = noDots;

void showDots()
{
    g_dots = doDots;
}
