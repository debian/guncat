#include "globals.ih"

#include <iostream>

string fileInfo()
{
    return "In " + g_filename + ", line " + to_string(g_lineNr);
}
