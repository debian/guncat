#define XERR
#include "gpghandler.ih"

void GPGHandler::childProcess()
{
    Pipe pipe;

                                        // d_filename: file to decrypt
xerr("CALLS " << d_gpgCommand << " and " << d_filename);

    if (d_filename.empty())
        throw 0;

    Process gpg{ 
                Process::DIRECT, Process::NO_PATH, 
                d_gpgCommand  + " --output " + d_decryptedName +
                passphraseFd(pipe) + " " + d_filename 
            };

    gpg.setTimeLimit(d_options.timeLimit());            // 0 -> no time limit

    gpg.start();
}



