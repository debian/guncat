#define XERR
#include "gpghandler.ih"

namespace
{
    size_t getCount = 0;
}

void GPGHandler::getPassphrase(size_t attempt)
{
    if (d_passphraseRead)
    {
        if (++getCount > 1)
            throw Exception{} << "password read from file is incorrect";
        return;
    }

    if (attempt > 1)
        cerr << "Incorrect passphrase.\n";

    cerr << "Enter passphrase (empty line to skip): ";

    d_passphrase.clear();

    struct termios ttySaved;

    int fd = echo(&ttySaved);       // switch off echoing, fd to /dev/tty

    IFdStream in(fd);               // get the passphrase from /dev/tty
    getline(in, d_passphrase);

    echo(&ttySaved, true);          // restore the echoing state

    cerr << '\n';

    if (d_passphrase.empty())
        throw Exception{} << "no passphrase entered";
}

