#ifndef INCLUDED_GPGHANDLER_
#define INCLUDED_GPGHANDLER_

#include <iosfwd>

#include <bobcat/fork>
#include <bobcat/pipe>

class Options;

class GPGHandler: public FBB::Fork
{
    Options const &d_options;

    std::ostream *d_messages;

    bool d_passphraseRead;                  // passphrase read from file
    std::string d_passphrase;

    std::string d_gpgCommand;
    std::string d_filename;
    std::string d_decryptedName;            // filename of the file to receive
                                            // the decrypted info

    FBB::Pipe d_childMessages;

    int d_ret;                              // return value of the gpg call

    static unsigned s_nRetries;             // # times a pwd was requested

    public:
        GPGHandler();  

        void setDecryptedName(std::string const &filename);

        int process(std::string const &filename);

        void getPassphrase(size_t attempt); // get the passphrase (/dev/tty
                                            // or from file)

    private:
        std::string passphraseFd(FBB::Pipe &pipe) const;

        void resetPipe();

        void childRedirections() override;
        void childProcess()      override;
        void parentProcess()     override;

        static int echo(struct termios *ttySaved, bool reset = false);
};
     
inline void GPGHandler::setDecryptedName(std::string const &filename)
{
    d_decryptedName = filename;
}
    
#endif









