#define XERR
#include "gpghandler.ih"

GPGHandler::GPGHandler()
:
    d_options(Options::instance()),
    d_messages(d_options.gpgMessages()),        
    d_gpgCommand(d_options.gpgPath() + d_options.gpgDecrypt())
{
    string const &path = d_options.passphrasePath();
    if (d_passphraseRead = not path.empty(); d_passphraseRead)
    {
        auto pwdStream = Exception::factory<ifstream>(path);
        getline(pwdStream, d_passphrase);
    }

//    xerr("passphraseread: " << d_passphraseRead << ": " << d_passphrase);
//    xerr("Initial GPG command: " << d_gpgCommand);
}

