#define XERR
#include "gpghandler.ih"

void GPGHandler::parentProcess()
{
    IFdStream childMessages(d_childMessages.readOnly(), 500);

    d_ret = waitForChild();

    if (d_messages)
        *d_messages << childMessages.rdbuf();

    if (d_ret == 0 and d_decryptedName != "-")
    {
//        xerr("processing IQuotedPrintable " << d_decryptedName);

        auto decrypted = Exception::factory<ifstream>(d_decryptedName);

        IQuotedPrintableBuf<DECODE> decode{ decrypted };
        istream din(&decode);

        cout << din.rdbuf();
    }
        
}




