#define XERR
#include "gpghandler.ih"

string GPGHandler::passphraseFd(Pipe &pipe) const
{
    xerr("passphraseread: " << d_passphraseRead << ": " << d_passphrase);

                                            // specify the fd receiving the
                                            // passphrase
    string ret{ 
                " --pinentry-mode loopback"
                " --passphrase-fd " + 
                to_string(pipe.readFd()) +
                ' '
            };

    OFdStream pwd(pipe.writeFd());          // create an ostream for it

                                            // write the passphrase, which
                                            // will be read by gpg from the
    pwd << d_passphrase << endl;            // pipe

    return ret;                             // return gpg's options
}
