#ifndef INCLUDED_GUNCAT_
#define INCLUDED_GUNCAT_

#include <iosfwd>
#include <fstream>
#include <string>
#include <memory>

#include "../decryptor/decryptor.h"
#include "../gpghandler/gpghandler.h"

class Options;

class Guncat
{
    Options const &d_options;

    bool d_gpgCommand;
    bool d_pgpRanges;
    bool d_passphraseFirstLine;                       // handled in process()
    bool d_reduceHeaders;

    GPGHandler d_gpgHandler;
    Decryptor d_decryptor;

    static char const *const s_accept[];
    static char const *const *const s_acceptEnd;

    public:
        Guncat();
        void run();

    private:
        void showGPGcommand() const;

        bool onlyCin();
        void processCin();

        void process(std::istream &in);
        void programArguments();

        bool reduceHeaders(std::string const &line);
};
        
#endif


