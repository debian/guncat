#define XERR
#include "guncat.ih"

void Guncat::process(istream &in)
{
    string line;

    g_lineNr = 0;

    bool reduce = false;

    while (nextline(in, line))
    {
        if (line == g_beginPGP)
        {
            d_decryptor.process(in);    // , d_out);
            continue;
        }

        if (d_pgpRanges)
            continue;

        if (not reduce and d_reduceHeaders and line.find("From ") == 0)
            reduce = true;

        if (reduce)
            reduce = reduceHeaders(line);
        else
            cout << line << '\n';
    }

    cout.flush();
}





