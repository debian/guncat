#include "main.ih"

int handle(exception_ptr ptr)
try
{
    rethrow_exception(ptr);
}
catch (int ret)                 // handle the known exceptions
{
    return Arg::instance().option("hv") ? 0 : ret;
}
catch (exception const &exc)
{
    cerr << "Error: " << exc.what() << '\n';
    return 1;
}
catch (...)                     // and handle an unexpected exception
{
    cerr << "unexpected exception\n";
    return 1;
}
