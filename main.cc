#include "main.ih"

// Options are defined in prepareargs.cc

namespace
{
    string terminating{ "Terminating" };
}

int main(int argc, char **argv)
try
{
    Arg const &arg = prepareArgs(argc, argv);

    (terminating += ' ') += arg.basename();

    arg.versionHelp(usage, Icmbuild::version, 0);

                                            // no stdin redirection, no files
    if (                                    // and not --gpg-command?
        not Options::instance().gpgCommand() and
            isatty(STDIN_FILENO) and arg.nArgs() == 0
    )
    {
        usage(arg.basename());
        throw 1;
    }
    
    Guncat guncat;
    guncat.run();                           // process all files

    if (arg.option('d'))                    // dots were requested
        cerr.put('\n');                     // then end the dots-line
}
catch (...)
{
    return handle(current_exception());
}
