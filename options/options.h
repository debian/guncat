#ifndef INCLUDED_OPTIONS_
#define INCLUDED_OPTIONS_

#include <string>
#include <fstream>
#include <memory>

namespace FBB
{
    class Arg;
}

class Options
{
    FBB::Arg const &d_arg;

    bool d_noErrors;
    bool d_gpgCommand;
    bool d_pgpRanges;
    bool d_sectionLines;
    bool d_reduceHeaders;
    bool d_skipIncomplete;
    bool d_quotedPrintable;

    std::string d_gpgDecrypt;               // options for decrypting
    std::string d_gpgPath; 
    std::string d_passphrasePath;           // file containing the passphrase
    std::string d_verbose;

    size_t d_timeLimit;
                                                    // a file by name 
    std::unique_ptr<std::ofstream> d_msgPtr;        // receiving GPG messages

    std::ostream *d_gpgMessages;                    // a pointer to the used
                                                    // messages stream

    
    static Options *s_options;

    public:
        static Options const &instance();

        bool gpgCommand() const;
        bool pgpRanges() const;             // only report PGP section ranges
        bool reduceHeaders() const;
        bool sectionLines() const;

                                            // skip incomplete PGP sections
        bool skipIncomplete() const;        // (implied by pgpRanges)

        bool quotedPrintable() const;

        bool noErrors() const;

        std::string const &gpgDecrypt() const;
        std::string const &gpgPath() const;

        std::ostream *const gpgMessages() const;

        std::string const &passphrasePath() const;
        std::string const &verbose() const;

        size_t timeLimit() const;

    private:
        Options();

        void extraGPGoptions();             // add --gpg-option values
        void setGpgMessages();
};
        
inline bool Options::gpgCommand() const
{
    return d_gpgCommand;
}
        
inline bool Options::pgpRanges() const
{
    return d_pgpRanges;
}

inline bool Options::reduceHeaders() const
{
    return d_reduceHeaders;
}

inline bool Options::sectionLines() const
{
    return d_sectionLines;
}

inline bool Options::quotedPrintable() const
{
    return d_quotedPrintable;
}
        
inline bool Options::skipIncomplete() const
{
    return d_skipIncomplete;
}
        
inline bool Options::noErrors() const
{
    return d_noErrors;
}

inline std::string const &Options::passphrasePath() const
{
    return d_passphrasePath;
}

inline std::ostream *const Options::gpgMessages() const
{
    return d_gpgMessages;
}

inline std::string const &Options::gpgDecrypt() const
{
    return d_gpgDecrypt;
}

inline std::string const &Options::gpgPath() const
{
    return d_gpgPath;
}

inline std::string const &Options::verbose() const
{
    return d_verbose;
}

inline size_t Options::timeLimit() const
{
    return d_timeLimit;
}

#endif





