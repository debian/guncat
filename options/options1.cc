#define XERR
#include "options.ih"

Options::Options()
:
    d_arg(Arg::instance()),
    d_noErrors(d_arg.option(0, "no-errors")),
    d_gpgCommand(d_arg.option(0, "gpg-command")),
    d_pgpRanges(d_arg.option('r')),
    d_sectionLines(d_arg.option('S')),
    d_reduceHeaders(d_arg.option('R')),
    d_skipIncomplete(d_pgpRanges or d_arg.option('s')),
    d_quotedPrintable(d_arg.option('q')),
    d_gpgDecrypt(" --no-auto-key-locate --decrypt --batch --yes")
{
    d_arg.option(&d_passphrasePath, "passphrase");

                                            // override default gpg location
    if (not d_arg.option(&d_gpgPath, "gpg-path"))    
        d_gpgPath = "/usr/bin/gpg";

                                            // get the name of the verbosity
                                            // file
    if (
        not d_arg.option(&d_verbose, 'V') and not d_sectionLines
                                          and not d_pgpRanges
    )
    {
        if (d_arg.option('d'))
            showDots();
    }

throw Exception{} << "verbose has value `" << d_verbose << '\'';

    if (not d_arg.option('t'))              // by default --no-tty is used
        d_gpgDecrypt += " --no-tty";

    if (string timeLimit; d_arg.option(&timeLimit, 'T'))
        d_timeLimit = stoul(timeLimit);    

    setGpgMessages();

    extraGPGoptions();                      // add extra gpg options            
}




