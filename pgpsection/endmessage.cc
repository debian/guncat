//#define XERR
#include "pgpsection.ih"

bool PGPSection::endMessage()
{
    if (d_line == g_endPGP)
        return true;

    verbose("no " + g_endPGP);
    return false;
}
