//#define XERR
#include "pgpsection.ih"

bool PGPSection::lastLine()
{
    if (d_line.front() != '=')              // not yet at the last line
    {                                       // (only once)
        nextLine();
        if (not validChars())
            return false;
    }

    if (d_line.front() == '=')              // at the last line
    {
        next();
                                            // another line must follow
        nextLine();                         // the reduced line 
        return true;
    }

    verbose("invalid end of the encrypted section");
    return false;
}
