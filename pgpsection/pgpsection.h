#ifndef INCLUDED_PGPSECTION_
#define INCLUDED_PGPSECTION_

#include <iosfwd>
#include <string>
#include <bobcat/tempstream>

    // determine the next pgp section and its line numbers

namespace FBB
{
    class TempStream;
}

class PGPSection
{
    FBB::TempStream &d_pgpMessage;

    std::istream &d_in;             // current input file

    size_t d_entryOffset;           // initial d_in offset         
    size_t d_offset;                // location (so far) in d_in

    size_t d_firstLine;             // first line nr. of a PGP section

                                    // line nr after recognizing a correct 
    size_t d_nextNr;                // PGP section element

    std::string d_line;             // line read by nextLine()
                        
                                    // true if the only the section lines
    bool d_pgpRanges;               // should be reported

    public:
        PGPSection(std::istream &in, FBB::TempStream &pgpMessage);

                                            // get a complete PGP MESSAGE 
        bool complete(  //std::ostream &out
                        );   // from the in-stream

                                    // line-range of the current PGP section
        std::pair<unsigned, unsigned> range() const;

    private:
                                    // in PGP section lines all chars must be 
        bool validChars() const;    // printable and may not be blanks
        bool nextLine();
        void next();

        bool empty();
        bool lines();
        bool lastLine();
        bool endMessage();

        void verbose(std::string const &what) const;
};
  
inline std::pair<unsigned, unsigned> PGPSection::range() const
{
    return { d_firstLine, d_nextNr + 1 };
}
      
#endif

