//#define XERR
#include "pgpsection.ih"

bool PGPSection::validChars() const
{
    for (int ch: d_line)
    {
        if (isblank(ch) or not isprint(ch))
        {
            verbose("invalid character(s) in the encrypted section");
            return false;
        }
    }

    return true;
}
